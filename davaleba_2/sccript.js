function cw2_1 (x) {
    y = Math.ceil(x)
    console.log(y)
    document.write("1) " + y)
}
cw2_1(34.12)

document.write("<br><hr>")


function cw2_2 (x) {
    y = Math.floor(x)
    console.log(y)
    document.write("2) " + y)
}
cw2_2(34.12)

document.write("<br><hr>")


function cw2_3 (x) {
    y = Math.round(x)
    console.log(y)
    document.write("3) " + y)
}
cw2_3(34.12)

document.write("<br><hr>")


function cw2_5 () {
    x = Math.random()
    console.log(x)
    document.write("5) " + x)
}
cw2_5()

document.write("<br><hr>")


function cw2_6__7 (a, b) {
    x = Math.random() * (a - b) + b
    console.log(x)
    document.write("6,7) " + x)
} 
cw2_6__7(5,50)

document.write("<br><hr>")


function cw2_8 (a, b) {
    x = Math.round(Math.random() * (a - b) + b)
    console.log(x)
    document.write("8) " + x)
} 
cw2_8(5,50)

document.write("<br><hr>")


function cw2_9 (a, b) {
    for (i = 0; i < 10; i++) {
        x = Math.round(Math.random() * (a - b) + b)
        console.log(x)
    document.write(x + "  ")
    }
}
cw2_9(10, 50)

document.write("<br><hr>")


function cw2_10 (a, b, n) {
    for (i = 0; i < n; i++) {
        x = Math.round(Math.random() * (a - b) + b)
        console.log(x)
    document.write(x + "  ")
    }
}
cw2_10 (50, 100, 5)

document.write("<br><hr>")


function cw2_18 () {
    t = "<table>"
        for (i=0; i<3; i++) {
            t += "<tr>"
                for (j=0; j<4; j++) {
                    t += "<td>"
                        r = Math.ceil(Math.random()*12)

                        img = "<img src='fotoebi/p" + r + ".jfif'/>"
                        t += img
                    t += "</td>"
                }
        }
    t += "</table>"
    console.log(t)
    document.write(t)
}

cw2_18()